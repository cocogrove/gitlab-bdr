# BDR Questionnaire

### 1. Why do you want to work as a BDR specifically for GitLab?
GitLab is an all-in-one integrated development tool quickly growing its services and user base, and there's still a lot of ground to cover. That's especially exciting for a BDR focused on selling a robust solution that's responsive to developer needs. I'd love to grow GitLab's client base and find avenues to push for new offerings. I'm also excited to work at a company that encourages employees to user their product.

### 2. What is your greatest professional accomplishment?
When I was an office manager at API.AI, I noticed the team wasn't optimizing the website for search engines. At the time, we didn't have any marketers, so I researched the benefits and best practices and took on SEO as a project. I increased our Moz page score from 61% to 93% after one month and put us on the first SERP for top keywords.

### 3. What motivates you?
I want people to be successful. Whether it's my users, coworkers, or friends, I thrive on researching and brainstorming ideas, finding answers, and being persistent about solutions.

### 4. How would you describe your knowledge of Developer Tools and/or Open Source Software?
I understand (and appreciate) the open source software concept - it's one of the reasons I like GitLab so much. As for developer tools, I've used python, Sublime, and SourceTree.

### 5. Where do you see your career in 5 years?
In five years I'd like to be working for a SaaS startup in an analytics role. I enjoy working in marketing and operations and could see myself in either area.